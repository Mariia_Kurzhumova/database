Створення додатку бази даних, орієнтованого на взаємодію з СУБД PostgreSQL
Варіант: сервіс продажу квитків кіно

Структура бази даних:
Сутність Films було перетворено в таблицю Films, сутність Sessions в таблицю Sessions, Hall в таблицю Hall, Seats/rows в таблицю Seats/rows, а 
зв’язок між сутностями Films та Sessions (M:N) в таблицю Film/Session.
(Синій – первинні ключі, зелений – зовнішні.)
Films: id(unique, not null, int)| name(not null, text)| description(not null text) | duration(not null, text) | year(not null, int) | rating(not null, text)
director(not null, text)
Sessions: id(unique, not null, int) | start_time(not null, time) | end_time(not null, time) | data(not null, data) | price(not null, money)
| hall_id(not null, int)
Hall: id(unique, not null, int) | number(not null, int) | amount_of_seats(not null, int) | type_of_hall(not null, text) | id_row_seat(not null, int)
Seats_rows: id(unique, not null, int) | row_number(not null, int) | seat_number(not null, int) | type_of_seat(not null, text)
Connections: id(unique, not null, int) | id_film(not null, int) | id_session(not null, int)
